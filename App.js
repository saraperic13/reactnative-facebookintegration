/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

const axios = require('axios');

export default class App extends Component{
  constructor(props) {
    super(props)

    this.state = {
      accessToken: null,
      id: null
    }
  }

  componentDidMount() {
     AccessToken.getCurrentAccessToken()
    .then((data) => {
      console.log("DATA", data);
      if(!data){
        return;
      }
      this.setState({
        accessToken: data.accessToken,
        id: data.userID
      });
      console.log("state", this.state);
      this.testRequestGraphAPI();
    })
    .catch(error => {
      console.log(error)
    })
  }

  testRequestGraphAPI = ()=>{  
    const infoRequest = new GraphRequest(
      `/${this.state.id}/friends`,
      // `/103233790985235/friends`,
      {
        parameters: {
          fields: {
            string: 'email,name,first_name,middle_name,last_name' // what you want to get
          },
          // access_token: {
          //   string: this.state.accessToken // put your accessToken here
          // }
        }
      },
      this._responseInfoCallback // make sure you define _responseInfoCallback in same class
    );
new GraphRequestManager().addRequest(infoRequest).start();
}
_responseInfoCallback = (error, result) => {
  if (error) {
    console.log(JSON.stringify(error));
  } else {
    console.log(JSON.stringify(result)); // print JSON
  } 
}

  render(){
    console.log("RENDER");
    return (
      <View style= {{marginTop: 200}}>
      <LoginButton
        readPermissions={["public_profile", "user_friends"]}
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                console.log("\n\nRESULT", result);
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    alert(data.accessToken.toString());
                    console.log("\n\nDATA", data);
                    this.getFriends(data.accessToken);
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")}/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
});